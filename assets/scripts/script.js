let grades = {
	"firstGrading": {
		"Math": 98.7,
		"English": 95.6,
		"Science": 98.5
	},

	"secondGrading": {
		"Math": 97.0,
		"English": 96.4,
		"Science": 94.7
	},

	"thirdGrading": {
		"Math": 98.1,
		"English": 95.1,
		"Science": 93.5
	},

	"fourthGrading": {
		"Math": 88.0,
		"English": 99.0,
		"Science": 90.0
	}
}

let studentInfo = {
	"fullName": "John Dominic Fandialan",
	"address": {
		"brgy": "457 San Benito",
		"city": "Alaminos",
		"province": "Laguna"
	},
	"age": 27,
	"grade": 8,
	"school": "University of Programming"
}

// //Number 1
// console.log("Here are the grades during the first and second grading");
// console.log("Math:");
// console.log("first grading:" + grades.firstGrading.Math);
// console.log("second grading:" + grades.secondGrading.Math);

// console.log("English:");
// console.log("first grading:" + grades.firstGrading.English);
// console.log("second grading:" + grades.secondGrading.English);

// console.log("Science:");
// console.log("first grading:" + grades.firstGrading.Science);
// console.log("second grading:" + grades.secondGrading.Science);

// console.log("Here are the grades during the third and fourth grading");
// console.log("Math:");
// console.log("third grading:" + grades.thirdGrading.Math);
// console.log("fourth grading:" + grades.fourthGrading.Math);

// console.log("English:");
// console.log("third grading:" + grades.thirdGrading.English);
// console.log("fourth grading:" + grades.fourthGrading.English);

// console.log("Science:");
// console.log("third grading:" + grades.thirdGrading.Science);
// console.log("fourth grading:" + grades.fourthGrading.Science);

// //#2
// console.log(`Hi, I am ${studentInfo.fullName}, currently taking programming at ${studentInfo.school} here in the PH. I live in ${studentInfo.address.brgy}, ${studentInfo.address.city}, ${studentInfo.address.province}. I am a grade ${studentInfo.grade} student. ${studentInfo.age} years of age.`);

// function createFullName(fullName, age, city, province){
// 	console.log(fullName);
// 	console.log(age + "y/o");
// 	console.log(city + " " + province);
// }

// createFullName("Jane Doe", 30, "Quezon City", "Metro Manila");

 function printGrade(grades){
 	console.log("Here are the grades during the first and second grading");
	console.log("Math:");
	console.log("first grading:" + grades.firstGrading.Math);
	console.log("second grading:" + grades.secondGrading.Math);

	console.log("English:");
	console.log("first grading:" + grades.firstGrading.English);
	console.log("second grading:" + grades.secondGrading.English);

	console.log("Science:");
	console.log("first grading:" + grades.firstGrading.Science);
	console.log("second grading:" + grades.secondGrading.Science);

	console.log("Here are the grades during the third and fourth grading");
	console.log("Math:");
	console.log("third grading:" + grades.thirdGrading.Math);
	console.log("fourth grading:" + grades.fourthGrading.Math);

	console.log("English:");
	console.log("third grading:" + grades.thirdGrading.English);
	console.log("fourth grading:" + grades.fourthGrading.English);

	console.log("Science:");
	console.log("third grading:" + grades.thirdGrading.Science);
	console.log("fourth grading:" + grades.fourthGrading.Science);
 }

 printGrade(grades);

function printStudentInfo(studentInfo){
	console.log(`Hi, I am ${studentInfo.fullName}, currently taking programming at ${studentInfo.school} here in the PH. I live in ${studentInfo.address.brgy}, ${studentInfo.address.city}, ${studentInfo.address.province}. I am a grade ${studentInfo.grade} student. ${studentInfo.age} years of age.`);
}

printStudentInfo(studentInfo);